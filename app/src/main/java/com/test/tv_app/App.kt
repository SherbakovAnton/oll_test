package com.test.tv_app

import android.app.Application
import com.test.tv_app.di.coreModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.KoinApplication
import org.koin.core.context.startKoin

class App: Application() {

    override fun onCreate() {
        super.onCreate()
        initDI()
    }

    private fun initDI(): KoinApplication = startKoin {
        androidContext(this@App)
        modules(coreModule)
    }

}
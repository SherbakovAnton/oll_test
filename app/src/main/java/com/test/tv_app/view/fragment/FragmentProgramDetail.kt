package com.test.tv_app.view.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.squareup.picasso.Picasso
import com.test.tv_app.R
import com.test.tv_app.viewmodel.ViewModelProgramDetail
import kotlinx.android.synthetic.main.fragment_program_detail.*
import org.koin.android.viewmodel.ext.android.sharedViewModel

class FragmentProgramDetail : Fragment(R.layout.fragment_program_detail) {

    private val programViewModel by sharedViewModel<ViewModelProgramDetail>()

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        programViewModel.proLiveData.observe(viewLifecycleOwner, Observer {
            Picasso.get().load(it?.icon).into(ivProgramImg)
            tvIdValue.text = getString(R.string.id_value) + it.id
            tvName.text = it.name
            tvDescription.text = it.description
        })
    }
}
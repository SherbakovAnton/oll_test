package com.test.tv_app.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.paging.ExperimentalPagingApi
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.test.tv_app.R
import com.test.tv_app.view.ProgramsAdapter

class FragmentLoading : Fragment(R.layout.fragment_loading)
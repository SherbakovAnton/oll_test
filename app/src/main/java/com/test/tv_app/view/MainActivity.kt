package com.test.tv_app.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.ParcelUuid
import android.util.Log
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.commit
import androidx.lifecycle.Observer
import androidx.paging.ExperimentalPagingApi
import com.test.tv_app.R
import com.test.tv_app.view.fragment.FragmentLoading
import com.test.tv_app.view.fragment.FragmentProgramDetail
import com.test.tv_app.view.fragment.FragmentPrograms
import com.test.tv_app.viewmodel.ViewModelDevice
import com.test.tv_app.viewmodel.ViewModelProgramDetail
import com.test.tv_app.viewmodel.ViewModelWeb
import org.koin.android.viewmodel.ext.android.viewModel

//http://oll.tv/demo?serial_number=&borderId=&direction=
class MainActivity : AppCompatActivity(R.layout.activity_main) {

    private val deviceInfoViewModel by viewModel<ViewModelDevice>()
    private val webViewModel        by viewModel<ViewModelWeb>()


    private val container           by lazy { R.id.container }

    /**************************** METHODS *****************************/

    @ExperimentalPagingApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initDeviceInfoLoad()
        setLoadingFragment()
        deviceInfoViewModel.deviceInfoLiveData.observe(this, Observer{
            Log.d("TVapp", it.uuid)
            onInitUUID(it.uuid)
        })

    }

    @ExperimentalPagingApi
    private fun onInitUUID(uuid: String) {
        webViewModel.fetchProgramsAsLiveData(uuid)
            .observe(this, Observer {
            Log.d("TVapp", "result = $it")
            setProgramsFragment()
        })
    }

    private fun setLoadingFragment() = supportFragmentManager.commit {
        replace(container, FragmentLoading())
    }

    @ExperimentalPagingApi
    private fun setProgramsFragment() = supportFragmentManager.commit {
        replace(container, FragmentPrograms())
    }

    private fun initDeviceInfoLoad() {
        deviceInfoViewModel.loadDeviceInfo()
    }
}

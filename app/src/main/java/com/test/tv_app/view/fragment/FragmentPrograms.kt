package com.test.tv_app.view.fragment

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.lifecycle.Observer
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.distinctUntilChanged
import androidx.lifecycle.lifecycleScope
import androidx.paging.ExperimentalPagingApi
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.test.tv_app.R
import com.test.tv_app.model.Program
import com.test.tv_app.view.ProgramsAdapter
import com.test.tv_app.viewmodel.ViewModelProgramDetail
import com.test.tv_app.viewmodel.ViewModelWeb
import kotlinx.android.synthetic.main.fragment_programs.*
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.sharedViewModel
import org.koin.android.viewmodel.ext.android.viewModel

@ExperimentalPagingApi
class FragmentPrograms : Fragment(R.layout.fragment_programs) {

    private val webViewModel        by viewModel<ViewModelWeb>()
    private val programViewModel    by sharedViewModel<ViewModelProgramDetail>()

    private val adapter: ProgramsAdapter by lazy { initAdapter() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        retainInstance = true
        setUpViews(view)

        webViewModel
            .sourceProduct
            .observe(viewLifecycleOwner, Observer {
                lifecycleScope.launch {
                    adapter.submitData(it)
                }
        })
    }

    private fun initAdapter() = ProgramsAdapter { program ->  setProgramDetailFragment(program) }

    private fun setProgramDetailFragment(program: Program?) = activity?.supportFragmentManager?.commit {
        programViewModel.setProgram(program)
        add(R.id.container, FragmentProgramDetail())
        addToBackStack(null)
    }

    private fun setUpViews(view: View) {
        rvProgram.layoutManager = LinearLayoutManager(context)
        rvProgram.adapter = adapter
    }
}
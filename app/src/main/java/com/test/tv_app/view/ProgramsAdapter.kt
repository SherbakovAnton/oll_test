package com.test.tv_app.view

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.test.tv_app.R
import com.test.tv_app.model.Program

class ProgramsAdapter(private val onClickListener: (Program?) -> Unit )
    : PagingDataAdapter<Program, RecyclerView.ViewHolder>(REPO_COMPARATOR) {

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as? ProgramViewHolder)?.bind(program = getItem(position))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            = ProgramViewHolder.getInstance(onClickListener, parent)

    class ProgramViewHolder(private val onClickListener: (Program?) -> Unit , view: View) : RecyclerView.ViewHolder(view) {

        private val programName     = view.findViewById<TextView>(R.id.tv_proggram_name)
        private val programImg      = view.findViewById<ImageView>(R.id.iv_img)
        private val root            = view.findViewById<View>(R.id.cl_root)

        companion object {

            fun getInstance(onClickListener: (Program?) -> Unit, parent: ViewGroup): ProgramViewHolder {
                val inflater = LayoutInflater.from(parent.context)
                val view = inflater.inflate(R.layout.item_program_view, parent, false)
                return ProgramViewHolder(onClickListener, view)
            }
        }

        fun bind(program: Program?) {
            Log.d("TVapp", "ID = ${program?.id}")

            programName.text = program?.name
            Picasso.get().load(program?.icon).into(programImg)
            root.setOnClickListener {
                onClickListener(program)
            }
        }
    }

    companion object {
        private val REPO_COMPARATOR = object : DiffUtil.ItemCallback<Program>() {

            override fun areItemsTheSame(oldItem: Program, newItem: Program): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Program, newItem: Program): Boolean =
                oldItem == newItem
        }
    }
}
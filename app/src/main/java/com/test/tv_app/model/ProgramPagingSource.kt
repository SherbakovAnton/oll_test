package com.test.tv_app.model

import androidx.paging.PagingSource
import com.github.kittinunf.fuel.core.HttpException
import com.test.tv_app.model.provider.WebProvider
import com.test.tv_app.model.provider.WebProvider.Companion.DEFAULT_PAGE_INDEX
import com.test.tv_app.model.provider.WebProvider.Companion.NEXT_PAGE_INDEX
import org.koin.java.KoinJavaComponent
import java.io.IOException

class ProgramPagingSource: PagingSource<Int, Program>() {

    private val webProvider by KoinJavaComponent.inject(WebProvider::class.java)



    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Program> {

        val page = params.key ?: DEFAULT_PAGE_INDEX

        return try {
            val response = webProvider.loadPrograms()
            LoadResult.Page(
                response, prevKey = if (page == DEFAULT_PAGE_INDEX) null else page - 1,
                nextKey = if (response.isEmpty()) null else page + 1
            )
        } catch (exception: IOException) {
            return LoadResult.Error(exception)
        } catch (exception: HttpException) {
            return LoadResult.Error(exception)
        }
    }
}
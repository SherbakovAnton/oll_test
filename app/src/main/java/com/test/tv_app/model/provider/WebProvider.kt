package com.test.tv_app.model.provider

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.liveData
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.coroutines.awaitObjectResponse
import com.github.kittinunf.fuel.coroutines.awaitObjectResult
import com.github.kittinunf.fuel.coroutines.awaitStringResponseResult
import com.github.kittinunf.fuel.rx.rxResponseObject
import com.github.kittinunf.fuel.rx.rxStringTriple
import com.github.kittinunf.result.Result
import com.test.tv_app.model.CategoryProgram
import com.test.tv_app.model.Program
import com.test.tv_app.model.ProgramPagingSource
import io.reactivex.Single
import kotlinx.coroutines.runBlocking

class WebProvider(val baseUrl: String) {

    lateinit var serialNumber: String

    companion object {

        const val DEFAULT_PAGE_INDEX = 0
        var NEXT_PAGE_INDEX = 0
        get() {
            return if (field == 0) {
                field = 1
                0
            } else field
        }
        internal var NEXT_BORDER_ID = 0
        const val DEFAULT_PAGE_SIZE = 40
    }

    fun loadChannel(
        pagingConfig: PagingConfig = getDefaultPageConfig()
    ): LiveData<PagingData<Program>> {

        return Pager(
            config = pagingConfig,
            pagingSourceFactory = { ProgramPagingSource() }
        ).liveData
    }

    fun loadChannel(
        serialNumber: String,
        pagingConfig: PagingConfig = getDefaultPageConfig()
    ): LiveData<PagingData<Program>> {
        this.serialNumber = serialNumber
        return Pager(
            config = pagingConfig,
            pagingSourceFactory = { ProgramPagingSource() }
        ).liveData
    }

    suspend fun loadPrograms() : List<Program> {
        return Fuel.get(
                baseUrl, listOf(
                    "serial_number" to serialNumber,
                    "borderId"      to NEXT_BORDER_ID,
                    "direction"     to NEXT_PAGE_INDEX
                )
            )
            .awaitObjectResult(CategoryProgram.Deserializer())
            .fold(
                {
                    val items = it.items
                    NEXT_BORDER_ID = items.last().id
                    return@fold items },
                {
                    Log.d("", "error request = $it")
                    return@fold emptyList<Program>()
                }
            )
    }

    fun getDefaultPageConfig(): PagingConfig {
        return PagingConfig(pageSize = DEFAULT_PAGE_SIZE, enablePlaceholders = true)
    }
}
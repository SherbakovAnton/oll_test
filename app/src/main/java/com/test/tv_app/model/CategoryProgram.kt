package com.test.tv_app.model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import java.io.Reader

data class CategoryProgram (
    @SerializedName("block_id")
    val blockId: String,
    @SerializedName("block_title")
    val blockTitle: String,
    @SerializedName("first_now_index")
    val firstNowIndex: Int,
    val hasMore: Int,
    val items: List<Program>
) {

    class Deserializer : ResponseDeserializable<CategoryProgram> {

        override fun deserialize(reader: Reader): CategoryProgram
                = Gson().fromJson(reader, CategoryProgram::class.java)
    }

}
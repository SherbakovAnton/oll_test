package com.test.tv_app.model.provider

import android.content.Context
import android.util.Log
import com.test.tv_app.BuildConfig
import java.util.*

class DataProvider(context: Context) {

    private val shared = context.getSharedPreferences(BuildConfig.SHARED_NAME, Context.MODE_PRIVATE)
    private val KEY_IDENTIFIER = "key_identifier"

    fun getDeviceIdentifier(): String {
        return runCatching {
            shared.getString(KEY_IDENTIFIER, null)!!
        }.getOrElse {
            val identifier = UUID.randomUUID().toString()
            safeIdentifier(identifier)
            return@getOrElse identifier
        }
    }

    private fun safeIdentifier(value: String) {
        shared.edit().putString(KEY_IDENTIFIER, value).apply()

    }

}
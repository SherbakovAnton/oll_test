package com.test.tv_app.model

data class Program(
    val id: Int,
    val name: String,
    val icon: String,
    val description: String
)
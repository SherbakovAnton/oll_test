package com.test.tv_app.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.test.tv_app.model.DeviceInfo
import com.test.tv_app.model.provider.DataProvider
import org.koin.java.KoinJavaComponent


class ViewModelDevice(application: Application): AndroidViewModel(application) {

    val deviceInfoLiveData = MutableLiveData<DeviceInfo>()

    private val dataProvider by KoinJavaComponent.inject(DataProvider::class.java)

    fun loadDeviceInfo() {
        val identifier = dataProvider.getDeviceIdentifier()
        val deviceInfo = DeviceInfo(identifier)
        deviceInfoLiveData.postValue(deviceInfo)
    }
}
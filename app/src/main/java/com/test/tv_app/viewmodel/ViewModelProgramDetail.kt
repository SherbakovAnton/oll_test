package com.test.tv_app.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.test.tv_app.model.Program

class ViewModelProgramDetail: ViewModel() {

    val proLiveData = MutableLiveData<Program>()

    fun setProgram(program: Program?) {
        program?.let { proLiveData.postValue(it) }
    }

}
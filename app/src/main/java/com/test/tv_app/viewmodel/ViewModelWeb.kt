package com.test.tv_app.viewmodel

import android.annotation.SuppressLint
import androidx.lifecycle.*
import androidx.paging.cachedIn
import androidx.paging.PagingData
import com.github.kittinunf.fuel.core.FuelError
import com.test.tv_app.model.CategoryProgram
import com.test.tv_app.model.Program
import com.test.tv_app.model.provider.DataProvider
import com.test.tv_app.model.provider.WebProvider
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import org.koin.java.KoinJavaComponent

class ViewModelWeb: ViewModel() {

    private val webProvider by KoinJavaComponent.inject(WebProvider::class.java)

    val sourceProduct: LiveData<PagingData<Program>> = webProvider.loadChannel().cachedIn(viewModelScope)

    fun fetchProgramsAsLiveData(uuid: String): LiveData<PagingData<Program>> {
        return webProvider.loadChannel(uuid)
            .cachedIn(viewModelScope)
    }
}
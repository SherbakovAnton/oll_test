package com.test.tv_app.di

import com.test.tv_app.BuildConfig
import com.test.tv_app.model.provider.DataProvider
import com.test.tv_app.model.provider.DeviceInfoProvider
import com.test.tv_app.model.provider.WebProvider
import com.test.tv_app.view.ProgramsAdapter
import com.test.tv_app.viewmodel.ViewModelDevice
import com.test.tv_app.viewmodel.ViewModelProgramDetail
import com.test.tv_app.viewmodel.ViewModelWeb
import org.koin.android.ext.koin.androidApplication
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val coreModule = module {

    viewModel { ViewModelDevice(androidApplication()) }
    viewModel { ViewModelWeb() }
    viewModel { ViewModelProgramDetail() }

    single { DeviceInfoProvider() }
    single { WebProvider(BuildConfig.SERVER_URL) }
    single { DataProvider(androidApplication()) }

}